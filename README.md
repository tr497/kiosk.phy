<img src=https://codeshare.phy.cam.ac.uk/itservices.phy/kiosk.phy/-/raw/master/common/kiosk.phy.png>

# kiosk.phy

The Raspberry Pi client for ooh.phy.

This is version 2.0 of the Kiosk for the Cavendish Out of Hours system, now working on the Raspberry Pi4 as well as the Pi3B+ and providing user feedback in the form of LED blinks and audible beeps.

The kiosk is now standalone running directly on Raspbian.  Any Raspberry Pi unit with the readers connected should be usable as a Kiosk.

The system has been used by various departments of the university during the COVID-19 pandemic for occcupancy monitoring.  It is effective with up to 150 people in a building, providing responsive sign-ins and outs.

There are many variations of how to build the readers and connect them to the device.  This can be done using a breadboard and the wiring diagram below.  There is also a guide on how to construct the Gurdon Institute's complete solution including detachable readers and 3D printable cases for the Raspberry Pis (Models 3B+ and 4) available here:


